// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAwvxID5DFwP3GMlJMC8mWUH6gIpDTFQo8",
    authDomain: "time-keeping-a1d2f.firebaseapp.com",
    databaseURL: "https://time-keeping-a1d2f.firebaseio.com",
    projectId: "time-keeping-a1d2f",
    storageBucket: "",
    messagingSenderId: "746939101106"
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
