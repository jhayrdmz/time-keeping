import { Component, ElementRef, ViewChild } from '@angular/core';
import { fromEvent, merge, of, Observable } from 'rxjs';
import { mapTo } from 'rxjs/operators';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import swal from 'sweetalert';
import { MatSnackBar } from '@angular/material';
import { ReversePipe } from 'ngx-pipes';

export interface Log { 
  employee_id: string;
  first_name: string;
  last_name: string;
  time_log: number;
  type: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ReversePipe]
})
export class AppComponent {
  @ViewChild('focus') private myFocus: ElementRef;
  online$: Observable<boolean>;
  private logsCollection: AngularFirestoreCollection<Log>;
  log: Observable<Log[]>;
  logs: any;
  employees: any;
  logType: any = 'time_in';
  loading: boolean = false;
  today: any;

  constructor(private db: AngularFirestore, public snackBar: MatSnackBar) {
    this.online$ = merge(
      of(navigator.onLine),
      fromEvent(window, 'online').pipe(mapTo(true)),
      fromEvent(window, 'offline').pipe(mapTo(false))
    );

    this.online$.subscribe(res => {
      if(res) {
        this.snackBar.open('Connected!', null, {
          duration: 5000
        });
      } else {
        this.snackBar.open('Connecting to server...');
      }
    });

    setInterval(() => {
      this.today = new Date();
      this.myFocus.nativeElement.focus();
    }, 1000);

    this.logsCollection = this.db.collection<Log>('timelog');
    this.logsCollection.valueChanges().subscribe((data: any) => {
      this.logs = data;
    });

    this.db.collection('employees').valueChanges().subscribe((data: any) => {
      this.employees = data;
    });
  }

  ngOnInit() {
  }

  matButtonToggle(val) {
    this.logType = val;
  }

  getEmployeeInfo(form) {
    let employee;
    let isExists = false;
    this.loading = true;

    this.employees.map(data => {
      if(data.id == form.value.employee_id) {
        isExists = true;
        employee = data;
      }
    });

    if(isExists) {
      swal({
        icon: 'info',
        title: `${employee.first_name} ${employee.last_name}`,
        text: `Are you sure you want to ${this.logType == 'time_in' ? 'Time In' : 'Time Out'}?`,
        buttons: {
          cancel: true,
          confirm: true
        }
      }).then(confirm => {
        if(confirm) {
          const id = this.db.createId();
          const log: Log = {
            employee_id: employee.id,
            first_name: employee.first_name,
            last_name: employee.last_name,
            time_log: Math.ceil(new Date().getTime() / 1000),
            type: this.logType
          };

          this.logsCollection.doc(id).set(log);

          swal("Success", `${this.logType == 'time_in' ? 'Time In' : 'Time Out'} was successfull!`, "success").then(res => {
            this.loading = false;
            form.resetForm();
          });
        } else {
          this.loading = false;
        }
      });
    } else {
      swal("Employee Not Found!", "Please try again.", "error").then(res => this.loading = false);
    }
  }
}
